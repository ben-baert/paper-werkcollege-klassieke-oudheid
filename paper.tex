\documentclass[a4paper]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern} % lettertype met alle karakters
\usepackage[dutch]{babel}
\usepackage[style=apa, natbib=true, sorting=nyt, firstinits=true, isbn=false, url=true]{biblatex}
\DeclareLanguageMapping{dutch}{dutch-apa}
\usepackage{csquotes}
\usepackage{url}
\bibliography{referenties.bib}

\begin{document}
\interfootnotelinepenalty=1000

\title{Vrijlating: hoe en waarom?}
\subtitle{Over de procedures en de motivaties van zowel eigenaren als keizers om slaven (niet) vrij te laten}
\author{Ben Baert}
\date{} 
\maketitle

\thispagestyle{empty}
\tableofcontents
\newpage
\setcounter{page}{1}

\section{Inleiding}
\subsection{Onderwerp}
Het onderwerp van deze paper is de procedure en motivering achter vrijlating in Rome. De kernvraag is in feite: waarom werden zoveel slaven vrijgelaten? Als er dan toch zoiets bestaat als slavernij, en je hebt slaven, waarom zou je ze dan vrijlaten? Om het met de woorden van Duff te zeggen, die overigens niet onbesproken is\footnote{Zie \cite[p.1]{mouritsen2011} en verder in deze paper}: ``Why did the Romans, a people far less humane than the Greeks, yet show greater liberality in the freeing of their slaves?''\footnote{\cite{duff1928}} Ook Keith Hopkins stelde een gelijkaardige vraag: ``Why did the Romans free so many slaves?''\footnote{\cite[p.115]{hopkins1978}}

Een open systeem waarbij er een constante stroom vrijlatingen is, zonder dat de slavernij zelf in vraag gesteld wordt, is vrij uniek in de geschiedenis. Speelt medelijden een rol, of spelen er toch ook vooral egoïstische motieven mee? Welke invloed hadden al deze vrijlatingen op de maatschappij en dan vooral op de perceptie van deze vrijgelatenen? Was men `blij' met zoveel vrijgelatenen en zoniet, welke gevolgen had dit?

De paper bestaat uit 3 delen.

Het eerste deel dient als situering en dus als voorbereiding op het verdere werk. Ik bespreek de rol van de slavernij in de Romeinse maatschappij en dan vooral de positie van de vrijgelatenen. Wat was hun sociale positie? Waren ze na hun vrijlating ook effectief `vrij'? Hoe was de band tussen de voormalige meester en zijn slaaf? Ik geef hier ook een kritiek op een louter legalistische benadering van de studie van de Romeinse slavernij en onderzoek een aantal mogelijke historiografische valkuilen. Ik besteed hier behoorlijk wat aandacht aan, omdat het hier toch gaat om een inleiding tot historisch onderzoek, waarbij het erg belangrijk is dat er voldoende aandacht is voor historiografie.

Een tweede deel gaat in op de procedures die vrijlating mogelijk maken. Zoals we zullen zien, waren deze procedures voorhanden, ten dele om op een eigenlijk bijna hypocriete manier de schijnbare tegenstelling tussen enerzijds de fundamentele kloof vrijgeborene - slaaf en anderzijds het fenomeen vrijlating te overbruggen.

Het derde en laatste deel gaat in op de mogelijke motivering die schuilgaat achter een vrijlating. Waarom zouden mensen in een maatschappij waar slavernij een enorm belangrijk deel van uitmaakt, ervoor kiezen om slaven vrij te laten? Het gaat hier, zoals we zullen zien, duidelijk niet om abolitionistische motivaties, maar wel om een constante stroom vrijlatingen, terwijl het instituut slavernij nooit in vraag gesteld wordt. Waarom dan toch vrijlaten? 

De paper concentreert zich op de periode van de 2de eeuw voor Christus tot de 2de eeuw na Christus, met nadruk op de overgangsperiode van republiek naar principaat, maar ook met hier en daar een verwijzing naar een vroegere of latere periode.

\subsection{Methodologische noot}
Het meest recente en naar mijn mening zonder twijfel ook het meest complete en meest objectieve basiswerk over vrijgelatenen en de procedures en motivaties erachter, is \textit{Freedmen in the Roman World} van Henrik Mouritsen. Dit boek heeft gediend als een introductie tot de materie, maar ook als aanzet tot het lezen van andere, gerelateerde boeken en artikels. 

Auteurs van primaire bronnen schrijven vaak over de wereld die ze zouden \textit{willen} zien, niet de wereld die er effectief is. Dit geldt echter zeker ook voor een aantal secundaire bronnen, vooral dan die bronnen die gepubliceerd werden in de 19de en eerste helft van de 20ste eeuw; zelfs heel wat werken uit de tweede helft van de 20ste eeuw hebben een aantal belangrijke problemen. Ik heb een -- achteraf bekeken vrij naïeve -- aanname gemaakt dat, omdat het al zo lang geleden is, moderne auteurs weinig ideologische projecties zouden maken.\footnote{Wat ik hier bedoel, is dat een moderne autur een pak minder dicht bij het Oude Rome staat dan bij pakweg de moderne slavernij in bijvoorbeeld de VS, de schuldvraag na WOII of landen die met hun koloniaal verleden in de knoop liggen (ik denk dan vooral aan Frankrijk).} Dit klopt absoluut niet; in feite heb ik geen enkel werk gevonden dat objectief en duidelijk een overzicht gaf van de materie, op Mouritsens boek na.\footnote{Ik heb het hier over overzichtswerken; er zijn wel artikels voorhanden die bepaalde aspecten belichten.} Ook verschillende reviewers zijn die mening toegedaan:

\begin{quote}

``At a time when the most important works on Roman freedmen still refer to “the decline of the Italian stock” and “the infiltration of the Roman population by foreigners,” Mouritsen’s comprehensive study of The Freedman in the Roman World represents an invaluable contribution''\footnote{\cite{reviewmouritsen1}}

``The analysis is nothing short of revolutionary, an important advance over the standard studies: A. M. Duff, Freedmen in the Early Roman Empire (Oxford: Clarendon Press, 1928); Susan Treggiari, Roman Freedmen during the Late Republic (Oxford: Clarendon Press, 1969); G. Fabre, Libertus: Recherches sur les rapports patron-affranchi à la fin de la République romaine (Rome: École française de Rome, 1981); and Wolfgang Waldstein, Operae  libertorum:  Untersuchungen  zur  Dienstpflicht  freigelassener  Sklaven (Stuttgart:  F. Steiner, 1986).''\footnote{\cite{reviewmouritsen2}}

\end{quote}

Op enkele uitzonderingen na, grijp ik niet terug naar deze problematische bronnen. Hoewel ze interessant kunnen zijn vanuit historiografisch oogpunt, zouden ze vooral problematisch zijn bij het schrijven van deze paper. Ze vallen vaak ergens tussen `niet objectief' en ronduit racistisch en ondersteunen vooral de agenda van de auteur, eerder dan een eerlijke geschiedkundige benadering. Overigens is ook Mouritsens boek niet 100\% volledig; zo zal hij bijvoorbeeld het afnemende belang van \textit{Manumissio Censu} onvoldoende benadrukken.\footnote{Zie hoofdstuk ~\ref{sec:procedures}} Over het algemeen gaat het wel over relatief kleine zaken, zeker geen grove feitenkwesties (voor zover ik heb kunnen vaststellen).

Qua antieke bronnen grijp ik vaak terug naar rechtsteksten. Hoewel een wet nooit een volledig verhaal biedt -- en ik dit ook meerdere malen benadruk in de paper, biedt het wel inzicht in wat er leeft op het moment dat de wet ingevoerd wordt. Ik probeer een sociale aanpak (literaire bronnen en ook moderne sociologische studies) te combineren met rechtsbronnen om inzicht te krijgen in wat er leefde in de periode die besproken wordt in de paper.

De antieke bronnen komen ofwel uit het \textit{Routledge Sourcebook on Greek and Roman Slavery} \citep{wiederman1989}, ofwel de Perseus Library Online. Het eerste boek is enerzijds weliswaar een goudmijn voor een student die onderzoek verricht over slavernij en aanverwante fenomenen, maar anderzijds veel problematischer dan ik initieel dacht. Teksten over Griekse en Romeinse slavernij worden door elkaar gepresenteerd, terwijl de Romeinse slavernij toch wel op een aantal punten fundamenteel verschilt, zoals op het vlak van vrijlating bijvoorbeeld. Er zijn uiteraard raakpunten, maar ze presenteren alsof er geen verschillen bestaan, is een academische uitgever onwaardig. Dit doet natuurlijk niks af aan de kwaliteit van de bronnen en de vertalingen, maar voorzichtigheid is dus wel geboden.

\newpage
\section{Een situering}
In dit deel zal ik trachten een korte situering van het fenomeen slavernij te geven, in functie van een verdere opbouw naar de motivering achter een mogelijke vrijlating van een slaaf. Dit deel heeft ook als doelstelling een mogelijk simplistische of zelfs misleidende historiografische benadering te vermijden.

\subsection{De Romeinse visie op slavernij}
Vanuit een strikt legalistische benadering zou ik dit hoofdstuk zeer kort kunnen maken. De volgende wettekst uit de \textit{Instituten} is duidelijk: ``The principal division of the law of persons is the following, namely, that all men are either free or slaves.''\footnote{Gaius, Instituten, 1.9} Je bent dus ofwel vrij, ofwel slaaf. Het ene is het tegenovergestelde van het andere. Het is dus een zeer fundamenteel onderscheid. Je vindt hier ook een aantal voorbeelden van uit bronnen over het dagdagelijkse leven in Rome. Een vrije kan geen slaaf worden:``id quidem quod ingenuis natura dedit, nulla fortunae iniuria eripi potest''\footnote{Quintilianus, Declamationes Minores 311 4.1}, zeker ook met de afschaffing van zelfverpachting met de wet \textit{Lex Poetelia Papiria}\footnote{Livius, boek 8, 28}, maar anderzijds kon men in uitzonderlijke gevallen wel tot slavernij veroordeeld worden.\footnote{Zie o.m. \cite{millar1984}} Dit is relevant omdat we hier zien dat het onderscheid zeer fundamenteel is, maar toch ook weer niet zo fundamenteel dat het niet overbrugd kon worden. In deze paper gaat het dan ook over de andere richting, namelijk van slaaf naar vrijgelatene.

Strikt genomen is de slaaf eigendom van zijn meester en stond hij onder \textit{ius mancipii}, net als vee of landbouwgrond.\footnote{\cite[p.13]{mouritsen2011}} Hij of zij had eigenlijk geen rechtpersoonlijkheid, kon niet wettelijk trouwen, enz.:

\begin{quote}

``Slaves are in the power of their owners. This power is derived from the common law of nations, for we can see that among all nations alike owners have the power of life and death over their slaves, and whatever is acquired by a slave is acquired on behalf of his owner..''\footnote{Gaius, Instituten, 1.52}

\end{quote}

We zien ook een gelijkaardige trend in de \textit{Lex Aquilla} (287 v. Chr.):

\begin{quote}

``It therefore appears that \emph{the law places in the same category with slaves animals} which are included under the head of cattle, and are kept in herds, as, for instance, sheep, goats, oxen, horses, mules, and asses. The question arises whether hogs are included under the designation of cattle, and it is very properly decided by Labeo that they are. Dogs, however, do not come under this head; and wild beasts are far from being included, as for instance, bears, lions, and panthers. Elephants and camels are, as it were, mixed, for they perform the labor of beasts of burden, and yet their nature is wild, and therefore they must be included in the first Section.'' \footnote{Gaius, Provinciaal Edict, boek VII, mijn nadruk}

\end{quote}



Dit moet wel al direct genuanceerd worden met een volgende passage:

\begin{quote}

``But nowadays neither Roman citizens nor any other people who are subject to the sovereignty of the Roman People have the right to treat their slaves with excessive and unreasonable brutality. For a Constitution of the Divine Emperor Antoninus orders anyone who kills his own slave without due reason to be brought to justice in exactly the same way as one who kills another's slave. Excessively harsh treatment on the part of owners is also limited by a Constitution of the same Emperor; for when certain provincial governors asked him for a ruling regarding slaves who had taken refuge at the temples of gods or statues of emperors, he declared that owners were to be forced to sell their slaves if the cruelty of their behaviour appeared to be unbearable. In both cases he ruled justly, for we ought not to misuse our rights—that is the ground for interdicting those who waste their own property from administering it.''\footnote{Gaius, Instituten, 1.53}

\end{quote}

Hoewel de slaaf dus eigendom was van zijn meester, net als land en vee, betekende dit niet dat alles was toegestaan. Let wel: deze tendens was pas merkbaar vanaf de opkomst van het principaat; tijdens de periode van de republiek waren er waarschijnlijk veel minder beschermingsmaatregelen. Bovendien zijn er, zoals ik reeds heb aangekondigd, een aantal problemen met een legalistische, essentialistische benadering. Ze is niet enkel onvolledig; ze kan het zelfs moeilijker maken om het fenomeen slavernij te begrijpen. Het is niet omdat een slaaf wettelijk gezien eigendom is (``een object''\footnote{\cite[pp. 139-143]{finley1998}}), dat men daarom zijn menselijkheid niet kon erkennen. Dit zien we ook in verschillende primaire bronnen. Zo zegt Plinius: ``in sickness there is no difference between slaves and freemen, yet give the latter milder and more gentle treatment''\footnote{Plinius, Ep.8, 24}, terwijl ``the minds and bodies of slaves are of the same substance, the same elements as ours''\footnote{Juvenalis, 14, 16-17}. Echt ontroerende teksten kan je het niet noemen, maar ze tonen wel aan dat slaven toch iets meer waren dan louter eigendom zoals een stuk land of vee dat waren. 

Mijn kritiek op de legalistische benadering is uiteraard niet nieuw en werd al aangehaald door o.m. Mouritsen\footnote{\cite[pp.13-14]{mouritsen2011}} en Patterson, die het nuttiger vond om te spreken van een concept van 'sociale dood'.\footnote{\cite{patterson1982}} Dit betekent dat een slaaf een persoon is die geen sociaal bestaan heeft en geen sociale rechten of banden heeft. De slaaf is dus `sociaal dood' maar is door zijn eigenaar gespaard gebleven: ``servitutem mortalitati fere comparamus''.\footnote{Ulpianus, Digesta, 50.17.209}

Belangrijk is ook om de verschillen tussen de perceptie van slavernij tussen de Grieken en de Romeinen op te merken. We kennen allemaal wel het fragment uit de \textit{Politeia} van Aristoteles:

\begin{quote}

``Therefore all men who differ from one another by as much as the soul differs from the body or man from a wild beast (and that is the state of those who work by using their bodies, and for whom that is the best they can do)—these people are slaves by nature, and it is better for them to be subject to this kind of control, as it is better for the other creatures I have mentioned. For a man who is able to belong to another person is by nature a slave (for that is why he belongs to someone else), as is a man who participates in reason only so far as to realise that it exists, but not so far as to have it himself—other animals do not recognise reason, but follow their passions.''\footnote{Aristoteles, Politeia, boek I, IV}

\end{quote}

Hoewel we ons kunnen afvragen in hoeverre deze visie van toepassing was op de Grieken zelf, is het vrijwel zeker dat ze in Rome weinig medestanders vond en heel wat tegenstanders. Zo zegt Florentinus dat dit idee net tegen de natuur ingaat.\footnote{Digesten, 1.5.4} Ook Seneca Maior zegt:

\begin{quote}

``No one is naturally free or a slave. These are titles imposed later on individuals.''\footnote{Controversiae, 7.6.18}

\end{quote}

Er bestond wel een concept tussen rechtvaardige en onrechtvaardige slavernij. Zo vinden we bij Livius:

\begin{quote}

``It was to the effect that where any of the Statellati who had made their surrender had not been restored to liberty by August 1, the senate should on oath empower a magistrate to seek out and punish the persons through whose criminal act they had passed into slavery. This order, thus sanctioned by the senate, was announced to the Assembly.''\footnote{Livius, Geschiedenis van Rome, Boek 42, hoofdstuk 21}

\end{quote}

Dit gebeurde omdat:

\begin{quote}

``the senate judged the war made on the Abderites to be unjust, and had directed that all those who were in servitude should be sought out and restored to liberty.''

\end{quote}

Als er slaven werden gevangen tijdens een onrechtvaardige oorlog, dan is ook die gevangenneming onrechtvaardig. Let wel: of een oorlog al dan niet onrechtvaardig was, hing niet zozeer af van morele, maar wel van zeer formalistische argumenten.

Op basis waarvan werd slavernij dan wel gerechtvaardigd? Volgens Gaius was het simpelweg het feit dat iedereen het aanvaardt, het \textit{ius gentium}.\footnote{Gaius, Instituten, 1.9, zie hiervoor.}

Om te concluderen, kan ik Watson citeren:

\begin{quote}

``To be a slave is a misfortune for the individual, assuredly a grave one, but it is not inevitable, natural, or necessarily permanent.''\footnote{\cite[p. 3]{watson1987}}

\end{quote}

Dat Watson gelijk heeft, bewijzen onder meer de grote aantallen vrijlatingen, die verder in deze paper zullen worden onderzocht.

\subsection{De positie en het statuut van de vrijgelatene}

Een slaaf die vrijgelaten wordt, wordt een vrijgelatene, wat onderscheiden wordt van een vrijgeborene en dit onderscheid uit zich zowel in slechts beperkte burgerrechten als in een stigma waarmee de vrijgelatene moet leven. Vanwaar het onderscheid? \textit{Macula servitutis}; een soort vlek door iemands achtergrond; het ooit geleefd te hebben in slavernij, verklaart het onderscheid. Het verklaart ook meteen waarom dat kinderen van vrijgelaten wel vrijgeborenen zijn met volledige burgerrechten. 

Door hun levensgeschiedenis en het onderscheid dat daaruit volgde, vormen de vrijgelatenen een aparte sociale klasse. Het was dus zeker niet zo dat als je vrijgelaten was, je ook verlost was van het stigma van ooit een slaaf geweest te zijn. 

Het is overigens ook belangrijk om op te merken dat een slaaf die vrijgelaten wordt, normaal gezien niet de band verbreekt met zijn voormalige meester, maar dat deze band wel hervormd wordt. De relatie tussen (voormalig) meester en slaaf blijft dus wel bestaan, maar verandert in een soort vader-zoon relatie. Waarom?

Zoals we gezien hebben, heeft de slaaf een aantal `gebreken', waaronder bijvoorbeeld een gebrek aan persoonlijkheid, waardoor hij nood heeft aan begeleiding in het `vrije leven'. Wie kan deze begeleiding beter geven dan de voormalige meester, hij die er overigens voor gezorgd heeft dat de slaaf een persoonlijke ontwikkeling kon bereiken die hem toeliet vrijgelaten te worden?\footnote{\cite[p.36]{mouritsen2011}}

Een vrijlating werd gezien als een geste van de meester (we zullen later zien dat de realiteit complexer is dan dit); een geste waar ook iets tegenover moest staan. Vanuit het concept van de slaaf die sociaal dood is\footnote{Zie de voorgaande paragrafen in deze sectie}, wordt de \textit{Pater familias} de redder van de voormalige slaaf; de patroon geeft de slaaf een sociaal bestaan. In die zin spreekt Mouritsen zelfs van een soort pseudo-geboorte.\footnote{\cite[p.38]{mouritsen2011}}

Het moge duidelijk zijn dat dit een, vanuit Romeins perspectief, bijzonder grote geste was. Daar stond wel iets tegenover; zo had de voormalige slaaf verdere verplichtingen tegenover zijn patroon. Ook waren er een aantal wetgevingen die duidelijk benadrukten dat de vaderfiguur van de patroon meer was dan louter metaforisch:

\begin{quote}

``When a freedman dies intestate, and does not leave any proper heir, but his patron, or the children of the latter survive him; the inheritance of the estate of the freedman shall be adjudged to the next of kin of the patron.'' \footnote{Twaalftafelenwet, tafel 5, wet 3}

\end{quote}

Vanuit de theorie dat de patroon een vader is, is deze wet totaal niet vreemd. Later wordt ze nog versterkt met de \textit{Lex Papia Poppaea} (9 na Chr.), een wet die als doel had huwelijken te bevorderen, door voorrechten te voorzien voor wettige kinderen en buitenechtelijke seks strafbaar te maken. 

\begin{quote}

``Subsequently, by the Lex Papia, the rights of patrons were increased, so far as the wealthier freedmen were concerned; for it is provided by this law that where a freedman left an estate of a hundred thousand sesterces, or more, and had less than three children, an equal share of his estate was due to the patron, whether he made a will or died intestate. Therefore, if a freedman should leave but one son or daughter, his patron will be entitled to half his estate, just as if he had died without leaving either a son or a daughter; and if he should leave two sons or two daughters, a third part of his estate will be due to the patron; but if he left three children, the patron will be excluded from the succession.''\footnote{Gaius, Instituten, 3.42}

\end{quote} 

Dit is dus een versterking van de twaalftafelenwet in het voordeel van de patroon, althans bij een voldoende rijke vrijgelatene en zolang die minder dan drie kinderen heeft.

Het is echter, uiteraard, ook niet allemaal negatief. De relatie tussen vrijgelatene en patroon was vaak positief, en vaak hadden patroons soms bijzonder veel vertrouwen in hun vrijgelaten slaaf, die ze vaak aanstelden als adviseur. Hieronder een brief van Cicero aan Tiro, een vrijgelaten slaaf:

\begin{quote}

``Warmest greeting from Tullius, his son, brother, and nephew to Tiro. [...] So I am now quite clear that, until you are entirely recovered, you should not risk a journey either by sea or land. I shall see you quite soon enough, if I see you thoroughly restored to health. [...] I do beg you, dear Tiro, not to spare expense in anything whatever necessary for your health. I have written to Curius to honour your draft to any amount: something, I think, ought to be paid to the doctor himself to make him more zealous. \emph{Your services to me are past counting—at home, in the forum, at Rome, in my province: in private and public business, in my literary studies and compositions.} But there is one service you can render me that will surpass them all-gratify my hopes by appearing before me well and strong! [...] Good-bye, dear Tiro, good-bye good-bye, and good health to you! Lepta and all the rest send their kind regards. Good-bye!''\footnote{Cicero, Familiares, 16.4, mijn nadruk}
\end{quote}

De brief is opgesteld zoals je een brief tussen vader en zoon mag verwachten en Cicero is duidelijk erg dankbaar voor alle diensten die Tiro als adviseur, op allerlei gebieden, hem heeft bewezen. 

Ook bekleedden vrijgelatenen en zeker hun kinderen -- die volledige burgerrechten bezaten -- vaak erg hoge posities. De positie van de vrijgelatene in het algemeen was dus vrij ambivalent. Enerzijds was er het stigma van voormalige slaaf, de \textit{Macula servitutis}, anderzijds was er de vaak positieve relatie tussen patroon en eigenaar en ook de vaak hoge posities die zij konden bekleedden. 

\newpage
\section{Hoe werden slaven vrijgelaten?}
\label{sec:procedures}
De bedoeling van dit hoofdstuk is tweeledig.

 In de eerste plaats wil ik een overzicht geven van de procedures die leidden tot de formele vrijlating van een slaaf, de zogenaamde \textit{Manumissio iusta}. Deze vormen van vrijlating staan in contrast met \textit{Manumissio minus iusta} -- informele vrijlating -- waarbij de slaaf slechts praktisch min of meer vrij was, maar terug in de slavernij geroepen kon worden.\footnote{Het is wel zo dat ook informele vrijlating gelegaliseerd wordt in de vroege keizertijd, en dat er hiermee ook een aantal beschermingsmaatregelingen werden ingevoerd die willekeur moesten tegengaan.}

Anderzijds wil ik met dit hoofdstuk ook de nadruk leggen op hoe gevoelig vrijlating wel lag, in de zin dat een aantal van deze procedures (Censu en Vindicta) eigenlijk schijngebaren waren, waarbij men zogezegd de slaaf in zijn oude, `juiste' status herstelt. 

\subsection{Manumissio vindicta}
Bij \textit{Manumission vindicta} wordt een schijnproces gehouden, waarbij een Romeins burger, de \textit{adsertor libertatis}, verklaart dat de slaaf eigenlijk een vrije is, en de eigenaar van de slaaf spreekt dit niet tegen. Daarop bevestigt de magistraat met \textit{imperium} de vrije status van de slaaf.\footnote{\cite[p.11]{mouritsen2011}}

Wat ik hier nogmaals wil benadruken, is het erg formalistische karakter ervan en ook hoeveel macht de eigenaar wel heeft. Hoewel er een magistraat met \textit{imperium} aanwezig is, is het de eigenaar die de macht heeft om burgerschap toe te kennen aan de slaaf.

\subsection{Censu}
\textit{Censu} houdt in dat een Censor de vrije status van een slaaf registreert. De census werd elke 5 jaar opgesteld en was een lijst van alle \textit{Civis} die gebruikt werd voor fiscale en militaire doeleinden. 

\begin{quote}

Those manumitted by the census are such as formerly, at the Lustral Census of Rome, by the order of their masters, offered themselves to be enrolled among Roman citizens.\footnote{Ulpianus, Regulae, 1.8}

\end{quote}

 Strikt genomen maakt de Censor van de slaaf geen vrijgelatene, maar registreert hij enkel de status als vrije:

\begin{quote}
The entry does not purport to make him a \textit{Civis}: It is a fictitious renewal of an entry, and the Censor is recording the fact that the man is a Civis, not making him one.\footnote{\cite[p.141]{buckland2001}}
\end{quote}

\textit{Censu} is wellicht de oudste methode en was hoofdzakelijk, net als de Censor zelf, een republikeins gebruik. Het lijkt erop dat deze methode wel nog bestond in de vroege keizertijd, maar dat het zeker tegen de latere keizertijd echt wel `uitgestorven' was.'\footnote{\cite[p.439-441]{buckland2001}}

\subsection{Testamento}
\textit{Testamento} is, zoals het woord al doet vermoeden, vrijlating door opname in het testament. Dit kan voorwaardelijk of onvoorwaardelijk. Onvoorwaardelijk betekent dat de slaaf na de dood van de meester simpelweg vrijkomt. In die zin is het ook iets speciaals, aangezien er dan geen patroon meer is om hem te begeleiden (tenzij iemand anders deze rol opneemt). Voorwaardelijk betekent dat er een aantal condities kunnen worden opgelegd, zoals bijvoorbeeld nog een tijd slaaf zijn in het huishouden vooraleer de vrijlating plaatsvindt.

\subsection{In Sacrosanctis Ecclesiis}

Dit is een beetje de vreemde eend in de bijt, in die zin dat deze procedure pas tot stand kwam in 313 na Christus. Hierbij kent de meester aan zijn voormalige slaaf, in de kerk en in het bijzijn van de andere christenen en een priester, de vrijheid toe aan de slaaf. Door dit publieke element, lijkt het de opvolger van de intussen uitgestorven \textit{Censu} methode, in die zin dat er een sterk element van publieke controle in vervat zit, wat we bij de andere vormen van vrijlating niet meer zien.\footnote{\cite[p.450 - 451]{buckland2001}}

\newpage
\section{Waarom werden slaven (niet meer) vrijgelaten?}
\subsection{Individuele motivaties}

Misschien wel dé belangrijkste onderzoeksvraag van deze paper is: waarom zou een meester zijn slaaf vrijlaten? 

Religieuze en/of filosofische motieven, zoals morele argumenten uit een Christelijk of Stoïcijns gedachtegoed, worden soms aangebracht als argument voor een humane omgang met slaven. Daar valt iets over te zeggen, aangezien we -- voor zover de statistieken van deze periode betrouwbaar zijn -- een terugval opmerken van het aantal slaven in de laatste eeuwen van het Romeinse Rijk\footnote{\cite[p.36]{philips1985}}, al valt natuurlijk de vraag te stellen of het hier gaat om een causaal verband en niet om louter irrelevante correlatie met de opkomst van het Christendom. Er lijken onvoldoende aanwijzingen te zijn dat filosofische en/of religieuze verklaringen algemeen genomen een voldoende verklaring bieden voor het fenomeen van het hoge aantal vrijlatingen.

Een eigenaar die een slaaf zijn vrijheid laat kopen, biedt een win-win voor beiden, maar toch vooral voor de eigenaar. Als een jonge slaaf 20 jaar lang werkt voor zijn eigenaar en in die periode voldoende verdient om zijn vrijheid te kopen, dan breekt er een periode aan waarin de eigenaar zich wellicht toch van de dan al oude en dus niet meer productieve slaaf zou willen ontdoen. Net op dat moment betaalt de slaaf voor zijn vrijheid en kan de eigenaar een jongere slaaf kopen. Bovendien heeft de slaaf gedurende die hele periode een incentive om hard te werken en kan de eigenaar zich 'goed voelen` bij het vrijlaten van zijn oude slaaf. In feite zijn er dus niks dan voordelen aan dit systeem verbonden voor de eigenaar. Het hoeft dan ook niet te verbazen dat dit zo populair was. \footnote{Zie o.m. \cite[p.35]{philips1985}, voor gelijkaardige ideeën.}

Wat uiteraard ook meespeelt, is de veranderende relatie tussen meester en slaaf. De slaaf heeft nog steeds verplichtingen ten opzichte van zijn meester (omgekeerd ook trouwens), terwijl de patroon wel een deel van de voedingskosten van de voormalige slaaf kan doorschuiven naar de overheid. 

Een persoonlijke affectie voor een bepaalde slaaf, in combinatie met de Romeinse visie dat mensen niet van nature slaaf zijn en vrijheid kunnen verdienen, kunnen ook een rol spelen. Dit zal uiteraard vaker van toepassing zijn op slaven die dicht bij hun meester staan, eerder dan op slaven die in slechte en vooral ook onpersoonlijke omstandigheden moesten werken, zoals in de mijnen bijvoorbeeld. 

Een combinatie en complexe interactie van al deze factoren is wellicht de verklaring voor het hoge aantal vrijlatingen in het Romeinse Rijk. Persoonlijke affectie voor een slaaf die hard zijn best doet, in combinatie met een financiële winst en een herdefiniëring van de relatie met de slaaf, die vaak ook nadien nog bijzonder nuttig werk kan verrichten, geven aan een eigenaar voldoende reden om vrijlating in bepaalde gevallen te overwegen. 

\subsection{Politieke motivaties: de Augustijnse hervormingen}

De Augustijnse hervormingen zijn de belangrijkste wijzigingen geweest in het wetgevend kader omtrent vrijlating. De belangrijkste vraag die we hier moeten stellen, is waarom de politiek zou willen ingrijpen.

Er zijn een aantal redenen. Een eerste ligt voor de hand: als een slaaf een vrijgelatene en dus burger wordt, heeft hij recht op het gratis graan. We hebben gezien dat dit een reden is voor een eigenaar om zijn slaaf vrij te laten, maar dan komt die kost uiteraard wel bij de overheid terecht. Deze kan dan reageren door het aantal vrijlatingen te beperken.\footnote{\cite[p.81]{mouritsen2011}}

Een meer fundamentele reden ligt wellicht in het in stand houden van het instituut slavernij zelf. Als er zeer veel vrijlatingen plaatsvinden, wordt de transitie tussen slaaf en vrijgelatene té evident en dit zou het hele concept in gevaar kunnen brengen. Zeker bij testamentaire manumissie, waarbij voormalige slaven \emph{zonder} patroon vrijgelaten worden, lijkt het onderscheid tussen de (voormalige) slaaf met zijn \textit{Macula servitutis} en de \textit{Ingenuus} volledig te verwateren. Zeker bij politieke onrust, waarbij mensen teruggrijpen naar fundamentele scheidingslijnen, is het belangrijk om deze duidelijk scheidingslijn in stand te houden. 

We zien dit idee bij Suetonius, die zegt:

\begin{quote}

``Considering it also of great importance to keep the people pure and unsullied by any taint of foreign or servile blood, he was most chary of conferring Roman citizenship and set a limit to manumission.''\footnote{Suetonius, Leven van Augustus, 40.3}

\end{quote}

Deze visie wordt ook gevolgd door moderne auteurs, die stelden dat Augstus de sociale orde wilde herstellen en bewaren.\footnote{Zie om \cite[p.97]{nicolet1984} en \cite[p.39]{gardner1993}}

\paragraph{Lex Fufia Caninia (2 v. Chr.)} De \textit{Lex Fufia Caninia} plaatste restricties op het aantal slaven dat kon vrijgelaten worden door een eigenaar en doet dat zeer gedetailleerd:


\begin{quote}

``Moreover, by the Lex Fufia Caninia a certain limit is established with reference to the manumission of slaves by a will.

Hence, he who has more than two slaves and not more than ten, is permitted to manumit as many as half of that number. He, however, who has more than ten and not more than thirty slaves, is permitted to manumit a third of that number; and he who has more than thirty slaves and not more than a hundred, is granted authority to manumit one fourth of his slaves. Finally, he who has more than one hundred and not more than five hundred, is not permitted to manumit more than a fifth; and, no matter how many slaves a man may have, he is not permitted to manumit more than this, as the law prescribes that no one shall have the right to manumit more than a hundred. Still, where anyone has only one or two slaves, his case does not come under this law, and therefore he has free power of manumission.''\footnote{Gaius, Instituten, 1.42-43}

\end{quote}

De Lex Fufia Caninia was een reactie op slaven die door testamentaire manumissie zouden vrijgelaten worden en dus (vaak) zonder patroon vrij zouden zijn.\footnote{Zie ook \cite[p.41]{gardner1993}} Zoals hiervoor vermeld, is dit type vrijlating bijzonder problematisch. 

Men kan zich wel enkele vragen stellen bij deze wet, vooral dan bij de eigenlijke impact van testamentaire manumissie. Aangezien testamentaire manumissie een grote impact heeft op de nabestaanden (die het met één of meerdere slaven minder moeten stellen), stelt Champlin de impact van dit fenomeen in vraag.\footnote{\cite[p.136-142]{champlin1991}}

Ook over de zogenaamde selectiecriteria kunnen vragen gesteld worden. Zo stelt Gardner dat de Lex Fufia Caninia wel een spreekt over selectie, maar dat er geen concrete criteria naar voren worden geschoven. Dit is wellicht omdat de wet vooral de \emph{indruk} van selectie wilde geven, eerder dan een eigenlijke selectie.\footnote{\cite[p.24]{gardner1991}}

\paragraph{Lex Aelia Sentia (4 na. Chr.)} 

De Lex Aelia Sentia stelde leeftijdslimieten in voor zowel slaveneigenaars als slaven zelf. Een slaafeigenaar moest voortaan minstens 20 jaar oud zijn en in goede geestelijke gezondheid verkeren. Een slaaf zelf moest 30 jaar oud zijn.\footnote{\cite[p.84]{mouritsen2011}}

Ook hier kan men zich weer de vraag stellen in hoeverre deze wet in de praktijk een effect had. Hoewel de niet onbesproken Duff spreekt van ``young boys of immature judgement recklessly throwing liberty and citizenship among their slaves''\footnote{\cite[p.32]{duff1928}}, is het waarschijnlijker dat het belang van dit soort vrijlatingen vrij beperkt was, niet in het minst omdat het sociaal vrij onaanvaardbaar was voor jonge meesters om hun slaven vrij te laten.\footnote{\cite[p.84]{mouritsen2011}}

Daar komt dan nog de \textit{Lex Iunia (19 na Chr.)} bovenop, die enig mogelijk effect van de twee bovenstaande wetten, en dan met name de Lex Aelia Sentia, nog verder uitholt door informele vrijlating officieel te erkennen. Hierdoor kunnen de criteria van de Lex Aelia Sentia ontweken worden, en kunnen ook jonge meester slaven informeel vrijlaten. Overigens waren er goede redenen om dit aspect van de wet door te voeren, aangezien er aan informele vrijlating geen burgerschap verbonden was (en dus geen gratis graan), maar wellicht wel de \textit{vicesima}, een belasting op vrijlating zelf.\footnote{\cite[p.85]{mouritsen2011}}

Men kan zich dus afvragen hoeveel impact deze wetten nu eigenlijk gehad hebben op het aantal vrijlatingen. Mijn eigen analyse, en hier volg ik Mouritsen\footnote{\cite[p.85]mouritsen2001}, is dat de impact bijzonder beperkt geweest zal zijn en misschien zelfs geïnterpreteerd kan worden als een politiek symbool:

``For that reason, Augustus' legislation may be best understood as a statement of principle rather than a radical attempt to alter current practice''\footnote{\cite[p.83]{mouritsen2011}}

\newpage
\section{Conclusie}

Vrijlating is een erg complex fenomeen. Er spelen zeer vele factoren, zowel individuele factoren als politieke factoren. Slaveneigenaars hebben, om financiële, filosofische of religieuze redenen, of omdat ze een band hebben opgebouwd met de slaaf, er vaak belang bij om een slaaf vrij te laten. Vaak speelt een combinatie van bovenstaande factoren een rol. Het is ook belangrijk dat een slaaf, behalve bij testamentaire manumissie (in de meeste gevallen), nog steeds verplichtingen had ten opzichte van zijn patroon.

Daar stond tegenover dat vrijgelatenen met de nodige argwaan werden bekeken, vooral in tijden van politieke onrust. Hun onvolmaaktheid als persoon zorgde ervoor dat men hen niet graag publieke en/of succesvolle rollen zag bekleden. De wetgeving hieromtrent is hier een afspiegeling van en speelde hierop in, door te pogen het aantal vrijlatingen, wellicht vrij onsuccesvol, te beperken. De wetgeving leek eerder principeel te zijn dan effectief verandering door te voeren. 

De vrijgelatene bekleedt dus een bijzondere rol binnen de Romeinse maatschappij. Enerzijds staat hij helemaal onderaan in de sociale hiërarchie en lijdt hij onder het stigma van voormalig slaaf te zijn, anderzijds bood een vrijlating ook heel wat kansen op succes en een positieve relatie met de patroon.
\newpage
\nocite{*} 
\printbibliography
\end{document}